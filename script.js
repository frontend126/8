'use strict';

const numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?", "");

const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
}

for (let i = 0; i < 2; i++) {
    const nameOfFilm = prompt("Один из последних просмотренных фильмов?", ""),
          rateFilm = prompt("На сколько оцените его?", "");

    if (nameOfFilm != null && rateFilm != null && nameOfFilm != '' && rateFilm != '' && nameOfFilm.length <= 50) {
        personalMovieDB.movies[nameOfFilm] = rateFilm;
    } else {
        console.log('error');
        i--;
    }


}

if (personalMovieDB.count < 10) {
    alert("Просмотрено довольно мало фильмов");
} else if (personalMovieDB.count >= 10 && personalMovieDB.count < 30) {
    alert("Вы классический зритель");
} else if (personalMovieDB.count >= 30) {
    alert("Вы киноман");
} else {
    alert("Произошла ошибка");
}

console.log(personalMovieDB); 

/*-----Второй способ----*/
/*
let i = 0;
while (i < 2) {
    const nameOfFilm = prompt("Один из последних просмотренных фильмов?", ""),
          rateFilm = prompt("На сколько оцените его?", "");

    if (nameOfFilm != null && rateFilm != null && nameOfFilm != '' && rateFilm != '' && nameOfFilm.length <= 50) {
        personalMovieDB.movies[nameOfFilm] = rateFilm;
        i++;
    } else {
        console.log('error');
    }
 }
*/


